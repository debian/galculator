# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Asier Iturralde Sarasola <asier.iturralde@gmail.com>, 2014-2015
msgid ""
msgstr ""
"Project-Id-Version: MATE Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-03-17 16:18+0100\n"
"PO-Revision-Date: 2015-03-21 16:55+0000\n"
"Last-Translator: Asier Iturralde Sarasola <asier.iturralde@gmail.com>\n"
"Language-Team: Basque (http://www.transifex.com/mate/MATE/language/eu/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: eu\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../galculator.desktop.in.h:1
msgid "Perform simple and scientific calculations"
msgstr ""

#: ../src/callbacks.c:1051
msgid "save here"
msgstr "gorde hemen"

#: ../src/callbacks.c:1427
#, c-format
msgid "[%s] on_prefs_number_combo_changed failed to retrieve iter. %s\n"
msgstr ""

#: ../src/callbacks.c:1590
#, c-format
msgid "[%s] on_main_window_check_resize should not get called. %s\n"
msgstr ""

#: ../src/main.c:61
#, c-format
msgid ""
"\n"
"%s v%s, (c) 2002-2014 Simon Flöry\n"
"\n"
"Usage: %s [options]\n"
"\n"
"options:\n"
"(GTK options)\n"
" -h, --help\t\tShow this usage message\n"
" -v, --version\t\tShow version information\n"
"\n"
"Compiled against GTK version %i.%i.%i\n"
"Linked against GTK version %i.%i.%i\n"
"%s-precision floating point numbers.\n"
msgstr ""

#: ../src/main.c:196
#, c-format
msgid "[%s] configuration file: Failed to create directory %s.\n"
msgstr ""

#: ../src/main.c:201
#, c-format
msgid ""
"[%s] configuration file: We will move the configuration file from %s to %s. "
"After you quit %s, you may remove the configuration file from its old "
"location %s.\n"
msgstr ""

#: ../src/galculator.h:92
msgid "Please submit a bugreport."
msgstr ""

#: ../src/calc_basic.c:300
#, c-format
msgid "[%s] %c - unknown operation character. %s\n"
msgstr ""

#: ../src/calc_basic.c:690
#, c-format
msgid ""
"[%s] Conversion of floating point number in float2string failed because buffer was too small. %s\n"
")"
msgstr ""

#: ../src/calc_basic.c:719
#, c-format
msgid ""
"[%s] Conversion of floating point number in float2stringP failed because buffer was too small. %s\n"
")"
msgstr ""

#: ../src/config_file.c:180
msgid "Pi"
msgstr "Pi"

#: ../src/config_file.c:181
msgid "pi"
msgstr "pi"

#: ../src/config_file.c:183
msgid "Euler's Number"
msgstr "Eulerren zenbakia"

#: ../src/config_file.c:184
msgid "e"
msgstr "e"

#: ../src/config_file.c:232
#, c-format
msgid "[%s] configuration file: ignoring unknown entry %s=%s. %s\n"
msgstr ""

#: ../src/config_file.c:256
#, c-format
msgid ""
"[%s] configuration file: %s has to be TRUE or FALSE. Using defaults. %s\n"
msgstr ""

#: ../src/config_file.c:262
#, c-format
msgid ""
"[%s] configuration file: failed to convert %s to a number properly. Have you"
" changed your locales? %s\n"
msgstr ""

#: ../src/config_file.c:265
#, c-format
msgid ""
"[%s] configuration file: ignoring unknown key_type in config structure. %s\n"
msgstr ""

#: ../src/config_file.c:284
#, c-format
msgid ""
"[%s] found unknown section %s in configuration file %s. Using preceding "
"section.\n"
msgstr ""

#: ../src/config_file.c:403
#, c-format
msgid ""
"[%s] configuration file: couldn't open configuration file %s for reading. "
"Nothing to worry about if you are starting %s for the first time. Using "
"defaults.\n"
msgstr ""

#: ../src/config_file.c:432
#, c-format
msgid ""
"[%s] configuration file - We reset at least one separator character as it "
"coincides with the decimal point. If you recently changed your locales "
"settings, this is nothing to worry about.\n"
msgstr ""

#: ../src/config_file.c:470
#, c-format
msgid ""
"[%s] configuration file: strange boolean when writing. Skipping this key. "
"%s\n"
msgstr ""

#: ../src/config_file.c:478
#, c-format
msgid ""
"[%s] configuration file: ignoring unknown \"key_type\" in "
"\"config_structure\". %s\n"
msgstr ""

#: ../src/config_file.c:501
#, c-format
msgid "[%s] configuration file: couldn't save/write to configuration file %s.\n"
msgstr ""

#: ../src/display.c:420
#, c-format
msgid ""
"[%s] Line_index exceeds valid range in function \"display_delete_line\". "
"%s\n"
msgstr ""

#: ../src/display.c:598
#, c-format
msgid "[%s] unknown display option in function \"display_change_option\""
msgstr ""

#: ../src/general_functions.c:43
msgid "unsupported inverse function"
msgstr ""

#: ../src/general_functions.c:49
msgid "unsupported hyperbolic function"
msgstr ""

#: ../src/general_functions.c:112
#, c-format
msgid "[%s] unknown notation mode in function \"all_clear\". %s\n"
msgstr ""

#: ../src/general_functions.c:138
#, c-format
msgid "[%s] failed to convert char %c in function \"axtof\". %s\n"
msgstr ""

#: ../src/general_functions.c:194
#, c-format
msgid "[%s] failed to convert %s in function \"ftoax\". %s\n"
msgstr ""

#: ../src/general_functions.c:274
#, c-format
msgid "[%s] failed to convert color %s in function \"set_button_color\". %s\n"
msgstr ""

#: ../src/general_functions.c:395
#, c-format
msgid "[%s] failed to find widget %s in function \"activate_menu_item\". %s\n"
msgstr ""

#: ../src/general_functions.c:437
msgid "unknown number base"
msgstr ""

#: ../src/general_functions.c:438
#, c-format
msgid "[%s] unknown number base in function \"get_display_number_string\". %s\n"
msgstr ""

#: ../src/general_functions.c:459
#, c-format
msgid "[%s] unknown number base in function \"get_display_number_length\". %s\n"
msgstr ""

#: ../src/general_functions.c:568
#, c-format
msgid ""
"[%s] failed to convert %s to a number properly in function "
"\"string2double\". Have you changed your locales? Deleting your "
"configuration file might solve this problem. %s\n"
msgstr ""

#: ../src/general_functions.c:586
#, c-format
msgid "[%s] unknown number base in function \"string2double\". %s\n"
msgstr ""

#: ../src/general_functions.c:741
#, c-format
msgid "[%s] unknown angle base in function \"x2rad\". %s\n"
msgstr ""

#: ../src/general_functions.c:756
#, c-format
msgid "[%s] unknown angle base in function \"rad2x\". %s\n"
msgstr ""

#: ../src/general_functions.c:773
#, c-format
msgid "[%s] unknown number base in function \"get_sep\". %s\n"
msgstr ""

#: ../src/general_functions.c:790
#, c-format
msgid "[%s] unknown number base in function \"get_sep_length\". %s\n"
msgstr ""

#: ../src/general_functions.c:807
#, c-format
msgid "[%s] unknown number base in function \"get_sep_char\". %s\n"
msgstr ""

#: ../src/general_functions.c:875
msgid "unknown display option in function \"change_option\""
msgstr ""

#: ../src/general_functions.c:915
#, c-format
msgid ""
"[%s] length of decimal point (in locale) is not supported: >%s<\n"
"You might face problems when using %s! %s\n"
")"
msgstr ""

#: ../src/ui.c:89
#, c-format
msgid ""
"[%s] Couldn't load %s. This file is necessary     to build galculator's user"
" interface. Make sure you did a make install and the file     is "
"accessible!\n"
msgstr ""

#: ../src/ui.c:959 ../ui/main_frame.ui.h:17 ../ui/main_frame_hildon.ui.h:16
msgid "Show _menu bar"
msgstr "Erakutsi _menu-barra"

#: ../src/ui.c:999
#, c-format
msgid "%s Preferences"
msgstr "%s hobespenak"

#: ../src/ui.c:1043 ../src/ui.c:1075
msgid "Name"
msgstr "Izena"

#: ../src/ui.c:1046
msgid "Value"
msgstr "Balioa"

#: ../src/ui.c:1049
msgid "Description"
msgstr "Deskribapena"

#: ../src/ui.c:1078
msgid "Variable"
msgstr "Aldagaia"

#: ../src/ui.c:1081
msgid "Expression"
msgstr "Adierazpena"

#: ../src/ui.c:1211 ../ui/basic_buttons_gtk2.ui.h:43
#: ../ui/basic_buttons_gtk3.ui.h:43 ../ui/scientific_buttons_gtk2.ui.h:98
#: ../ui/scientific_buttons_gtk3.ui.h:98
msgid "="
msgstr "="

#: ../src/ui.c:1211 ../src/ui.c:1223 ../ui/basic_buttons_gtk2.ui.h:44
#: ../ui/basic_buttons_gtk3.ui.h:44 ../ui/paper_view.ui.h:2
#: ../ui/scientific_buttons_gtk2.ui.h:99 ../ui/scientific_buttons_gtk3.ui.h:99
msgid "Enter"
msgstr "Sartu"

#: ../src/ui.c:1213 ../ui/scientific_buttons_gtk2.ui.h:17
#: ../ui/scientific_buttons_gtk3.ui.h:17
msgid "x^y"
msgstr "x^y"

#: ../src/ui.c:1213 ../src/ui.c:1225 ../ui/scientific_buttons_gtk2.ui.h:18
#: ../ui/scientific_buttons_gtk3.ui.h:18
msgid "Power"
msgstr "Berredura"

#: ../src/ui.c:1215 ../ui/basic_buttons_gtk2.ui.h:33
#: ../ui/basic_buttons_gtk3.ui.h:33 ../ui/scientific_buttons_gtk2.ui.h:88
#: ../ui/scientific_buttons_gtk3.ui.h:88
msgid "("
msgstr "("

#: ../src/ui.c:1215 ../ui/basic_buttons_gtk2.ui.h:34
#: ../ui/basic_buttons_gtk3.ui.h:34 ../ui/scientific_buttons_gtk2.ui.h:89
#: ../ui/scientific_buttons_gtk3.ui.h:89
msgid "Open Bracket"
msgstr "Ireki parentesia"

#: ../src/ui.c:1217 ../ui/basic_buttons_gtk2.ui.h:31
#: ../ui/basic_buttons_gtk3.ui.h:31 ../ui/scientific_buttons_gtk2.ui.h:86
#: ../ui/scientific_buttons_gtk3.ui.h:86
msgid ")"
msgstr ")"

#: ../src/ui.c:1217 ../ui/basic_buttons_gtk2.ui.h:32
#: ../ui/basic_buttons_gtk3.ui.h:32 ../ui/scientific_buttons_gtk2.ui.h:87
#: ../ui/scientific_buttons_gtk3.ui.h:87
msgid "Close Bracket"
msgstr "Itxi parentesia"

#: ../src/ui.c:1223
msgid "ENT"
msgstr ""

#: ../src/ui.c:1225
msgid "y^x"
msgstr "y^x"

#: ../src/ui.c:1227
msgid "x<>y"
msgstr "x<>y"

#: ../src/ui.c:1227
msgid "swap current number with top of stack"
msgstr ""

#: ../src/ui.c:1229
msgid "roll"
msgstr ""

#: ../src/ui.c:1229
msgid "roll down stack"
msgstr ""

#: ../src/ui.c:1300
msgid "Result Display"
msgstr ""

#: ../src/flex_parser.l:233
#, c-format
msgid ""
"[%s] flex parser was called more than %i times. Do you know what you are "
"doing? If not: %s\n"
msgstr ""

#: ../ui/about.ui.in.h:1
msgid "About galculator"
msgstr "galculator-i buruz"

#: ../ui/about.ui.in.h:2
msgid "Copyright (c) 2002-2014 by Simon Flöry <simon.floery@rechenraum.com>"
msgstr "Copyright-a (c) 2002-2014 Simon Flöry <simon.floery@rechenraum.com>"

#: ../ui/about.ui.in.h:3
msgid "a GTK+ based scientific calculator"
msgstr "GTK+en oinarrituriko kalkulagailu zientifikoa"

#: ../ui/basic_buttons_gtk2.ui.h:1 ../ui/basic_buttons_gtk3.ui.h:1
#: ../ui/scientific_buttons_gtk2.ui.h:52 ../ui/scientific_buttons_gtk3.ui.h:52
msgid "7"
msgstr "7"

#: ../ui/basic_buttons_gtk2.ui.h:2 ../ui/basic_buttons_gtk3.ui.h:2
#: ../ui/scientific_buttons_gtk2.ui.h:53 ../ui/scientific_buttons_gtk3.ui.h:53
msgid "Seven"
msgstr "Zazpi"

#: ../ui/basic_buttons_gtk2.ui.h:3 ../ui/basic_buttons_gtk3.ui.h:3
#: ../ui/scientific_buttons_gtk2.ui.h:54 ../ui/scientific_buttons_gtk3.ui.h:54
msgid "8"
msgstr "8"

#: ../ui/basic_buttons_gtk2.ui.h:4 ../ui/basic_buttons_gtk3.ui.h:4
#: ../ui/scientific_buttons_gtk2.ui.h:55 ../ui/scientific_buttons_gtk3.ui.h:55
msgid "Eight"
msgstr "Zortzi"

#: ../ui/basic_buttons_gtk2.ui.h:5 ../ui/basic_buttons_gtk3.ui.h:5
#: ../ui/scientific_buttons_gtk2.ui.h:56 ../ui/scientific_buttons_gtk3.ui.h:56
msgid "9"
msgstr "9"

#: ../ui/basic_buttons_gtk2.ui.h:6 ../ui/basic_buttons_gtk3.ui.h:6
#: ../ui/scientific_buttons_gtk2.ui.h:57 ../ui/scientific_buttons_gtk3.ui.h:57
msgid "Nine"
msgstr "Bederatzi"

#: ../ui/basic_buttons_gtk2.ui.h:7 ../ui/basic_buttons_gtk3.ui.h:7
#: ../ui/scientific_buttons_gtk2.ui.h:58 ../ui/scientific_buttons_gtk3.ui.h:58
msgid "/"
msgstr "/"

#: ../ui/basic_buttons_gtk2.ui.h:8 ../ui/basic_buttons_gtk3.ui.h:8
#: ../ui/scientific_buttons_gtk2.ui.h:59 ../ui/scientific_buttons_gtk3.ui.h:59
msgid "Divide"
msgstr "Zatiketa"

#: ../ui/basic_buttons_gtk2.ui.h:9 ../ui/basic_buttons_gtk3.ui.h:9
#: ../ui/scientific_buttons_gtk2.ui.h:62 ../ui/scientific_buttons_gtk3.ui.h:62
msgid "4"
msgstr "4"

#: ../ui/basic_buttons_gtk2.ui.h:10 ../ui/basic_buttons_gtk3.ui.h:10
#: ../ui/scientific_buttons_gtk2.ui.h:63 ../ui/scientific_buttons_gtk3.ui.h:63
msgid "Four"
msgstr "Lau"

#: ../ui/basic_buttons_gtk2.ui.h:11 ../ui/basic_buttons_gtk3.ui.h:11
#: ../ui/scientific_buttons_gtk2.ui.h:64 ../ui/scientific_buttons_gtk3.ui.h:64
msgid "5"
msgstr "5"

#: ../ui/basic_buttons_gtk2.ui.h:12 ../ui/basic_buttons_gtk3.ui.h:12
#: ../ui/scientific_buttons_gtk2.ui.h:65 ../ui/scientific_buttons_gtk3.ui.h:65
msgid "Five"
msgstr "Bost"

#: ../ui/basic_buttons_gtk2.ui.h:13 ../ui/basic_buttons_gtk3.ui.h:13
#: ../ui/scientific_buttons_gtk2.ui.h:66 ../ui/scientific_buttons_gtk3.ui.h:66
msgid "6"
msgstr "6"

#: ../ui/basic_buttons_gtk2.ui.h:14 ../ui/basic_buttons_gtk3.ui.h:14
#: ../ui/scientific_buttons_gtk2.ui.h:67 ../ui/scientific_buttons_gtk3.ui.h:67
msgid "Six"
msgstr "Sei"

#: ../ui/basic_buttons_gtk2.ui.h:15 ../ui/basic_buttons_gtk3.ui.h:15
#: ../ui/scientific_buttons_gtk2.ui.h:68 ../ui/scientific_buttons_gtk3.ui.h:68
msgid "3"
msgstr "3"

#: ../ui/basic_buttons_gtk2.ui.h:16 ../ui/basic_buttons_gtk3.ui.h:16
#: ../ui/scientific_buttons_gtk2.ui.h:69 ../ui/scientific_buttons_gtk3.ui.h:69
msgid "Three"
msgstr "Hiru"

#: ../ui/basic_buttons_gtk2.ui.h:17 ../ui/basic_buttons_gtk3.ui.h:17
#: ../ui/scientific_buttons_gtk2.ui.h:70 ../ui/scientific_buttons_gtk3.ui.h:70
msgid "-"
msgstr "-"

#: ../ui/basic_buttons_gtk2.ui.h:18 ../ui/basic_buttons_gtk3.ui.h:18
#: ../ui/scientific_buttons_gtk2.ui.h:71 ../ui/scientific_buttons_gtk3.ui.h:71
msgid "Subtract"
msgstr "Kenketa"

#: ../ui/basic_buttons_gtk2.ui.h:19 ../ui/basic_buttons_gtk3.ui.h:19
#: ../ui/scientific_buttons_gtk2.ui.h:72 ../ui/scientific_buttons_gtk3.ui.h:72
msgid "+"
msgstr "+"

#: ../ui/basic_buttons_gtk2.ui.h:20 ../ui/basic_buttons_gtk3.ui.h:20
#: ../ui/scientific_buttons_gtk2.ui.h:73 ../ui/scientific_buttons_gtk3.ui.h:73
msgid "Sum"
msgstr "Batuketa"

#: ../ui/basic_buttons_gtk2.ui.h:21 ../ui/basic_buttons_gtk3.ui.h:21
#: ../ui/scientific_buttons_gtk2.ui.h:76 ../ui/scientific_buttons_gtk3.ui.h:76
msgid "+/-"
msgstr "+/-"

#: ../ui/basic_buttons_gtk2.ui.h:22 ../ui/basic_buttons_gtk3.ui.h:22
#: ../ui/scientific_buttons_gtk2.ui.h:77 ../ui/scientific_buttons_gtk3.ui.h:77
msgid "Change sign"
msgstr "Aldatu zeinua"

#: ../ui/basic_buttons_gtk2.ui.h:23 ../ui/basic_buttons_gtk3.ui.h:23
#: ../ui/main_frame_hildon.ui.h:34 ../ui/scientific_buttons_gtk2.ui.h:78
#: ../ui/scientific_buttons_gtk3.ui.h:78
msgid "*"
msgstr "*"

#: ../ui/basic_buttons_gtk2.ui.h:24 ../ui/basic_buttons_gtk3.ui.h:24
#: ../ui/scientific_buttons_gtk2.ui.h:79 ../ui/scientific_buttons_gtk3.ui.h:79
msgid "Multiply"
msgstr "Biderketa"

#: ../ui/basic_buttons_gtk2.ui.h:25 ../ui/basic_buttons_gtk3.ui.h:25
#: ../ui/scientific_buttons_gtk2.ui.h:80 ../ui/scientific_buttons_gtk3.ui.h:80
msgid "Add current display value to memory"
msgstr "Gehitu bistaraturiko balioa memoriara"

#: ../ui/basic_buttons_gtk2.ui.h:26 ../ui/basic_buttons_gtk3.ui.h:26
#: ../ui/scientific_buttons_gtk2.ui.h:81 ../ui/scientific_buttons_gtk3.ui.h:81
msgid "M+"
msgstr "M+"

#: ../ui/basic_buttons_gtk2.ui.h:27 ../ui/basic_buttons_gtk3.ui.h:27
#: ../ui/scientific_buttons_gtk2.ui.h:82 ../ui/scientific_buttons_gtk3.ui.h:82
msgid "Read from memory"
msgstr "Irakurri memoriatik"

#: ../ui/basic_buttons_gtk2.ui.h:28 ../ui/basic_buttons_gtk3.ui.h:28
#: ../ui/scientific_buttons_gtk2.ui.h:83 ../ui/scientific_buttons_gtk3.ui.h:83
msgid "MR"
msgstr "MR"

#: ../ui/basic_buttons_gtk2.ui.h:29 ../ui/basic_buttons_gtk3.ui.h:29
#: ../ui/scientific_buttons_gtk2.ui.h:84 ../ui/scientific_buttons_gtk3.ui.h:84
msgid "Save to memory"
msgstr "Gorde memoriara"

#: ../ui/basic_buttons_gtk2.ui.h:30 ../ui/basic_buttons_gtk3.ui.h:30
#: ../ui/scientific_buttons_gtk2.ui.h:85 ../ui/scientific_buttons_gtk3.ui.h:85
msgid "MS"
msgstr "MS"

#: ../ui/basic_buttons_gtk2.ui.h:35 ../ui/basic_buttons_gtk3.ui.h:35
#: ../ui/scientific_buttons_gtk2.ui.h:90 ../ui/scientific_buttons_gtk3.ui.h:90
msgid "0"
msgstr "0"

#: ../ui/basic_buttons_gtk2.ui.h:36 ../ui/basic_buttons_gtk3.ui.h:36
#: ../ui/scientific_buttons_gtk2.ui.h:91 ../ui/scientific_buttons_gtk3.ui.h:91
msgid "Zero"
msgstr "Zero"

#: ../ui/basic_buttons_gtk2.ui.h:37 ../ui/basic_buttons_gtk3.ui.h:37
#: ../ui/scientific_buttons_gtk2.ui.h:92 ../ui/scientific_buttons_gtk3.ui.h:92
msgid "."
msgstr "."

#: ../ui/basic_buttons_gtk2.ui.h:38 ../ui/basic_buttons_gtk3.ui.h:38
#: ../ui/scientific_buttons_gtk2.ui.h:93 ../ui/scientific_buttons_gtk3.ui.h:93
msgid "Decimal point"
msgstr "Hamartarren bereizlea"

#: ../ui/basic_buttons_gtk2.ui.h:39 ../ui/basic_buttons_gtk3.ui.h:39
#: ../ui/scientific_buttons_gtk2.ui.h:94 ../ui/scientific_buttons_gtk3.ui.h:94
msgid "1"
msgstr "1"

#: ../ui/basic_buttons_gtk2.ui.h:40 ../ui/basic_buttons_gtk3.ui.h:40
#: ../ui/scientific_buttons_gtk2.ui.h:95 ../ui/scientific_buttons_gtk3.ui.h:95
msgid "One"
msgstr "Bat"

#: ../ui/basic_buttons_gtk2.ui.h:41 ../ui/basic_buttons_gtk3.ui.h:41
#: ../ui/scientific_buttons_gtk2.ui.h:96 ../ui/scientific_buttons_gtk3.ui.h:96
msgid "2"
msgstr "2"

#: ../ui/basic_buttons_gtk2.ui.h:42 ../ui/basic_buttons_gtk3.ui.h:42
#: ../ui/scientific_buttons_gtk2.ui.h:97 ../ui/scientific_buttons_gtk3.ui.h:97
msgid "Two"
msgstr "Bi"

#: ../ui/basic_buttons_gtk2.ui.h:45 ../ui/basic_buttons_gtk3.ui.h:45
#: ../ui/scientific_buttons_gtk2.ui.h:5 ../ui/scientific_buttons_gtk3.ui.h:5
msgid "sqrt"
msgstr "sqrt"

#: ../ui/basic_buttons_gtk2.ui.h:46 ../ui/basic_buttons_gtk3.ui.h:46
#: ../ui/scientific_buttons_gtk2.ui.h:6 ../ui/scientific_buttons_gtk3.ui.h:6
msgid "Square root"
msgstr "Erro karratua"

#: ../ui/basic_buttons_gtk2.ui.h:48 ../ui/basic_buttons_gtk3.ui.h:48
#: ../ui/scientific_buttons_gtk2.ui.h:24 ../ui/scientific_buttons_gtk3.ui.h:24
#, no-c-format
msgid "%"
msgstr "%"

#: ../ui/basic_buttons_gtk2.ui.h:49 ../ui/basic_buttons_gtk3.ui.h:49
#: ../ui/scientific_buttons_gtk2.ui.h:25 ../ui/scientific_buttons_gtk3.ui.h:25
msgid "Percent"
msgstr "Ehunekoa"

#: ../ui/classic_view.ui.h:1 ../ui/main_frame.ui.h:1
#: ../ui/main_frame_hildon.ui.h:35 ../ui/paper_view.ui.h:1
msgid "The main program window."
msgstr ""

#: ../ui/classic_view.ui.h:2 ../ui/main_frame_hildon.ui.h:33
msgid "Formula entry text field - enter string expression to compute here."
msgstr ""

#: ../ui/dispctrl_bottom_gtk2.ui.h:1 ../ui/dispctrl_bottom_gtk3.ui.h:1
#: ../ui/dispctrl_right_gtk2.ui.h:1 ../ui/dispctrl_right_gtk3.ui.h:1
#: ../ui/dispctrl_right_vertical_gtk2.ui.h:5
#: ../ui/dispctrl_right_vertical_gtk3.ui.h:5
msgid "AC"
msgstr "AC"

#: ../ui/dispctrl_bottom_gtk2.ui.h:2 ../ui/dispctrl_bottom_gtk3.ui.h:2
#: ../ui/dispctrl_right_gtk2.ui.h:2 ../ui/dispctrl_right_gtk3.ui.h:2
#: ../ui/dispctrl_right_vertical_gtk2.ui.h:6
#: ../ui/dispctrl_right_vertical_gtk3.ui.h:6
msgid "Reset calculator (clear all)"
msgstr "Berrezarri kalkulagailua (garbitu dena)"

#: ../ui/dispctrl_bottom_gtk2.ui.h:3 ../ui/dispctrl_bottom_gtk3.ui.h:3
#: ../ui/dispctrl_right_gtk2.ui.h:5 ../ui/dispctrl_right_gtk3.ui.h:5
#: ../ui/dispctrl_right_vertical_gtk2.ui.h:3
#: ../ui/dispctrl_right_vertical_gtk3.ui.h:3
#: ../ui/scientific_buttons_gtk2.ui.h:40 ../ui/scientific_buttons_gtk3.ui.h:40
msgid "C"
msgstr "C"

#: ../ui/dispctrl_bottom_gtk2.ui.h:4 ../ui/dispctrl_bottom_gtk3.ui.h:4
#: ../ui/dispctrl_right_gtk2.ui.h:6 ../ui/dispctrl_right_gtk3.ui.h:6
#: ../ui/dispctrl_right_vertical_gtk2.ui.h:4
#: ../ui/dispctrl_right_vertical_gtk3.ui.h:4
msgid "Clear display"
msgstr "Garbitu pantaila"

#: ../ui/dispctrl_bottom_gtk2.ui.h:5 ../ui/dispctrl_bottom_gtk3.ui.h:5
#: ../ui/dispctrl_right_gtk2.ui.h:3 ../ui/dispctrl_right_gtk3.ui.h:3
#: ../ui/dispctrl_right_vertical_gtk2.ui.h:1
#: ../ui/dispctrl_right_vertical_gtk3.ui.h:1
msgid "<-"
msgstr ""

#: ../ui/dispctrl_bottom_gtk2.ui.h:6 ../ui/dispctrl_bottom_gtk3.ui.h:6
#: ../ui/dispctrl_right_gtk2.ui.h:4 ../ui/dispctrl_right_gtk3.ui.h:4
#: ../ui/dispctrl_right_vertical_gtk2.ui.h:2
#: ../ui/dispctrl_right_vertical_gtk3.ui.h:2
msgid "Backspace"
msgstr ""

#: ../ui/main_frame.ui.h:2 ../ui/main_frame_hildon.ui.h:1
msgid "_File"
msgstr "_Fitxategia"

#: ../ui/main_frame.ui.h:3 ../ui/main_frame_hildon.ui.h:3
msgid "_Edit"
msgstr "_Editatu"

#: ../ui/main_frame.ui.h:4 ../ui/main_frame_hildon.ui.h:4
msgid "Cu_t Display Value"
msgstr "_Moztu bistaratutako balioa"

#: ../ui/main_frame.ui.h:5 ../ui/main_frame_hildon.ui.h:5
msgid "_Copy Display Value"
msgstr "_Kopiatu bistaratutako balioa"

#: ../ui/main_frame.ui.h:6
msgid "_Paste and Append to Display Value"
msgstr "_Itsatsi eta gehitu bistaratutako balioari"

#: ../ui/main_frame.ui.h:7 ../ui/main_frame_hildon.ui.h:7
msgid "_Preferences..."
msgstr "_Hobespenak..."

#: ../ui/main_frame.ui.h:8 ../ui/main_frame_hildon.ui.h:8
msgid "_View"
msgstr "_Ikusi"

#: ../ui/main_frame.ui.h:9 ../ui/main_frame_hildon.ui.h:9
msgid "_Basic Mode"
msgstr "_Oinarrizko modua"

#: ../ui/main_frame.ui.h:10 ../ui/main_frame_hildon.ui.h:10
msgid "_Scientific Mode"
msgstr "Modu _zientifikoa"

#: ../ui/main_frame.ui.h:11
msgid "_Paper Mode"
msgstr "_Paperezko modua"

#: ../ui/main_frame.ui.h:12 ../ui/main_frame_hildon.ui.h:11
#: ../ui/prefs_gtk2.ui.h:40 ../ui/prefs_gtk3.ui.h:40 ../ui/prefs-ume.ui.h:19
msgid "Buttons"
msgstr "Botoiak"

#: ../ui/main_frame.ui.h:13 ../ui/main_frame_hildon.ui.h:12
msgid "Display Control"
msgstr "Bistaratu kontrola"

#: ../ui/main_frame.ui.h:14 ../ui/main_frame_hildon.ui.h:13
#: ../ui/prefs_gtk2.ui.h:53 ../ui/prefs_gtk3.ui.h:53
msgid "Functions"
msgstr "Funtzioak"

#: ../ui/main_frame.ui.h:15 ../ui/main_frame_hildon.ui.h:14
msgid "Logical"
msgstr "Logika"

#: ../ui/main_frame.ui.h:16 ../ui/main_frame_hildon.ui.h:15
msgid "Standard"
msgstr "Estandarra"

#: ../ui/main_frame.ui.h:18 ../ui/main_frame_hildon.ui.h:17
msgid "_Calculator"
msgstr "_Kalkulagailua"

#: ../ui/main_frame.ui.h:19 ../ui/main_frame_hildon.ui.h:18
msgid "_Number bases"
msgstr "_Zenbaki-oinarria"

#: ../ui/main_frame.ui.h:20 ../ui/main_frame_hildon.ui.h:19
msgid "_Decimal"
msgstr "_Hamartarra"

#: ../ui/main_frame.ui.h:21 ../ui/main_frame_hildon.ui.h:20
msgid "_Hexadecimal"
msgstr "Hama_seitarra"

#: ../ui/main_frame.ui.h:22 ../ui/main_frame_hildon.ui.h:21
msgid "_Octal"
msgstr "_Zortzitarra"

#: ../ui/main_frame.ui.h:23 ../ui/main_frame_hildon.ui.h:22
msgid "_Binary"
msgstr "_Bitarra"

#: ../ui/main_frame.ui.h:24 ../ui/main_frame_hildon.ui.h:23
msgid "Angle _units"
msgstr "Angelu unitateak"

#: ../ui/main_frame.ui.h:25 ../ui/main_frame_hildon.ui.h:24
msgid "D_egrees"
msgstr "_Graduak"

#: ../ui/main_frame.ui.h:26 ../ui/main_frame_hildon.ui.h:25
msgid "_Radians"
msgstr "_Radianak"

#: ../ui/main_frame.ui.h:27 ../ui/main_frame_hildon.ui.h:26
msgid "_Gradians"
msgstr "_Gradianak"

#: ../ui/main_frame.ui.h:28 ../ui/main_frame_hildon.ui.h:27
msgid "Notation _modes"
msgstr "Notazio _moduak"

#: ../ui/main_frame.ui.h:29 ../ui/main_frame_hildon.ui.h:28
msgid "_Algebraic"
msgstr "_Aljebraikoa"

#: ../ui/main_frame.ui.h:30 ../ui/main_frame_hildon.ui.h:29
msgid "_Reverse Polish (RPN)"
msgstr "_Alderantzizko poloniarra (RPN)"

#: ../ui/main_frame.ui.h:31 ../ui/main_frame_hildon.ui.h:30
msgid "_Formula Entry"
msgstr "_Formula sarrera"

#: ../ui/main_frame.ui.h:32 ../ui/main_frame_hildon.ui.h:31
msgid "_Help"
msgstr "_Laguntza"

#: ../ui/main_frame_hildon.ui.h:2
msgid "_Quit"
msgstr "_Irten"

#: ../ui/main_frame_hildon.ui.h:6
msgid "_Paste As Display Value"
msgstr "_Itsatsi bistaratutako balio bezala"

#: ../ui/main_frame_hildon.ui.h:32
msgid "_About..."
msgstr "_Honi buruz..."

#: ../ui/prefs_gtk2.ui.h:1 ../ui/prefs_gtk3.ui.h:1
msgid "Decimal"
msgstr "Hamartarra"

#: ../ui/prefs_gtk2.ui.h:2 ../ui/prefs_gtk3.ui.h:2
msgid "Hexadecimal"
msgstr "Hamaseitarra"

#: ../ui/prefs_gtk2.ui.h:3 ../ui/prefs_gtk3.ui.h:3
msgid "Octal"
msgstr "Zortzitarra"

#: ../ui/prefs_gtk2.ui.h:4 ../ui/prefs_gtk3.ui.h:4
msgid "Binary"
msgstr "Bitarra"

#: ../ui/prefs_gtk2.ui.h:5 ../ui/prefs_gtk3.ui.h:5 ../ui/prefs-ume.ui.h:1
msgid "galculator - Preferences"
msgstr "galculator - Hobespenak"

#: ../ui/prefs_gtk2.ui.h:6 ../ui/prefs_gtk3.ui.h:6 ../ui/prefs-ume.ui.h:2
msgid "A dialog that is used to change preferences settings."
msgstr ""

#: ../ui/prefs_gtk2.ui.h:7 ../ui/prefs_gtk3.ui.h:7 ../ui/prefs-ume.ui.h:3
msgid "Closes the dialog."
msgstr "Elkarrizketa-koadroa ixten du."

#: ../ui/prefs_gtk2.ui.h:8 ../ui/prefs_gtk3.ui.h:8
msgid "<b>Appearance</b>"
msgstr "<b>Itxura</b>"

#: ../ui/prefs_gtk2.ui.h:9 ../ui/prefs_gtk3.ui.h:9 ../ui/prefs-ume.ui.h:5
msgid "    "
msgstr "    "

#: ../ui/prefs_gtk2.ui.h:10 ../ui/prefs_gtk3.ui.h:10
msgid "Result font:"
msgstr "Emaitzaren letra-tipoa:"

#: ../ui/prefs_gtk2.ui.h:11 ../ui/prefs_gtk3.ui.h:11
msgid "Inactive module color:"
msgstr "Modulu inaktiboen kolorea:"

#: ../ui/prefs_gtk2.ui.h:12 ../ui/prefs_gtk3.ui.h:12
msgid "Active module color:"
msgstr "Modulu aktiboen kolorea:"

#: ../ui/prefs_gtk2.ui.h:13 ../ui/prefs_gtk3.ui.h:13
msgid "Result font color:"
msgstr "Emaitzaren letra-kolorea:"

#: ../ui/prefs_gtk2.ui.h:14 ../ui/prefs_gtk3.ui.h:14
msgid "Background color:"
msgstr "Atzeko planoko kolorea:"

#: ../ui/prefs_gtk2.ui.h:15 ../ui/prefs_gtk3.ui.h:15
msgid "Module font:"
msgstr "Moduluaren letra-tipoa:"

#: ../ui/prefs_gtk2.ui.h:16 ../ui/prefs_gtk3.ui.h:16
msgid "RPN stack font:"
msgstr "RPN pilaren letra-tipoa:"

#: ../ui/prefs_gtk2.ui.h:17 ../ui/prefs_gtk3.ui.h:17
msgid "RPN stack color:"
msgstr "RPN pilaren kolorea:"

#: ../ui/prefs_gtk2.ui.h:18 ../ui/prefs_gtk3.ui.h:18
msgid "Pick a Display Background Color"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:19 ../ui/prefs_gtk3.ui.h:19
msgid "Pick a Display Result Font Color"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:20 ../ui/prefs_gtk3.ui.h:20
msgid "Pick a RPN Stack Color"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:21 ../ui/prefs_gtk3.ui.h:21
msgid "Pick a Inactive Module Color"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:22 ../ui/prefs_gtk3.ui.h:22
msgid "Pick a Active Module Color"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:23 ../ui/prefs_gtk3.ui.h:23
msgid "Pick a Result Font"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:24 ../ui/prefs_gtk3.ui.h:24
msgid "Pick a RPN Stack Font"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:25 ../ui/prefs_gtk3.ui.h:25
msgid "Pick a Module Font"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:26 ../ui/prefs_gtk3.ui.h:26 ../ui/prefs-ume.ui.h:4
msgid "<b>Modules Visibility</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:27 ../ui/prefs_gtk3.ui.h:27 ../ui/prefs-ume.ui.h:6
msgid "Angle base units"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:28 ../ui/prefs_gtk3.ui.h:28 ../ui/prefs-ume.ui.h:7
msgid "Notation"
msgstr "Notazioa"

#: ../ui/prefs_gtk2.ui.h:29 ../ui/prefs_gtk3.ui.h:29 ../ui/prefs-ume.ui.h:8
msgid "Arithmetic operation"
msgstr "Eragiketa aritmetikoa"

#: ../ui/prefs_gtk2.ui.h:30 ../ui/prefs_gtk3.ui.h:30 ../ui/prefs-ume.ui.h:9
msgid "Open brackets"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:31 ../ui/prefs_gtk3.ui.h:31 ../ui/prefs-ume.ui.h:10
msgid "Number base units"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:32 ../ui/prefs_gtk3.ui.h:32 ../ui/prefs-ume.ui.h:11
msgid "Display"
msgstr "Bistaratu"

#: ../ui/prefs_gtk2.ui.h:33 ../ui/prefs_gtk3.ui.h:33 ../ui/prefs-ume.ui.h:12
msgid "<b>Font</b>"
msgstr "<b>Letra-tipoa</b>"

#: ../ui/prefs_gtk2.ui.h:34 ../ui/prefs_gtk3.ui.h:34 ../ui/prefs-ume.ui.h:13
msgid "Use custom button font"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:35 ../ui/prefs_gtk3.ui.h:35 ../ui/prefs-ume.ui.h:14
msgid "Button font:"
msgstr "Botoien letra-tipoa:"

#: ../ui/prefs_gtk2.ui.h:36 ../ui/prefs_gtk3.ui.h:36 ../ui/prefs-ume.ui.h:15
msgid "Pick a Button Font"
msgstr "Aukeratu botoien letra-tipoa"

#: ../ui/prefs_gtk2.ui.h:37 ../ui/prefs_gtk3.ui.h:37 ../ui/prefs-ume.ui.h:16
msgid "<b>Dimension</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:38 ../ui/prefs_gtk3.ui.h:38 ../ui/prefs-ume.ui.h:17
msgid "Minimal button width:"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:39 ../ui/prefs_gtk3.ui.h:39 ../ui/prefs-ume.ui.h:18
msgid "Minimal button height:"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:41 ../ui/prefs_gtk3.ui.h:41
msgid "<b>User defined constants:</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:42 ../ui/prefs_gtk3.ui.h:42
msgid "Name:"
msgstr "Izena:"

#: ../ui/prefs_gtk2.ui.h:43 ../ui/prefs_gtk3.ui.h:43
msgid "Value:"
msgstr "Balioa:"

#: ../ui/prefs_gtk2.ui.h:44 ../ui/prefs_gtk3.ui.h:44
msgid "Description:"
msgstr "Deskribapena:"

#: ../ui/prefs_gtk2.ui.h:45 ../ui/prefs_gtk3.ui.h:45
msgid "Add"
msgstr "Gehitu"

#: ../ui/prefs_gtk2.ui.h:46 ../ui/prefs_gtk3.ui.h:46
msgid "Update"
msgstr "Eguneratu"

#: ../ui/prefs_gtk2.ui.h:47 ../ui/prefs_gtk3.ui.h:47
msgid "Delete"
msgstr "Ezabatu"

#: ../ui/prefs_gtk2.ui.h:48 ../ui/prefs_gtk3.ui.h:48
msgid "Clear"
msgstr "Garbitu"

#: ../ui/prefs_gtk2.ui.h:49 ../ui/prefs_gtk3.ui.h:49
#: ../ui/scientific_buttons_gtk2.ui.h:21 ../ui/scientific_buttons_gtk3.ui.h:21
msgid "Constants"
msgstr "Konstanteak"

#: ../ui/prefs_gtk2.ui.h:50 ../ui/prefs_gtk3.ui.h:50
msgid "<b>User defined functions:</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:51 ../ui/prefs_gtk3.ui.h:51
msgid "Variable:"
msgstr "Aldagaia:"

#: ../ui/prefs_gtk2.ui.h:52 ../ui/prefs_gtk3.ui.h:52
msgid "Expression:"
msgstr "Adierazpena:"

#: ../ui/prefs_gtk2.ui.h:54 ../ui/prefs_gtk3.ui.h:54 ../ui/prefs-ume.ui.h:20
msgid "Configure number base: "
msgstr ""

#: ../ui/prefs_gtk2.ui.h:55 ../ui/prefs_gtk3.ui.h:55
msgid "<b>Decimal Settings</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:56 ../ui/prefs_gtk3.ui.h:56
msgid "Show thousands separator"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:57 ../ui/prefs_gtk3.ui.h:57
msgid "Separator character:"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:58 ../ui/prefs_gtk3.ui.h:58
msgid "Separator block length:"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:59 ../ui/prefs_gtk3.ui.h:59
msgid " "
msgstr " "

#: ../ui/prefs_gtk2.ui.h:60 ../ui/prefs_gtk3.ui.h:60
msgid "<b>Hexadecimal Settings</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:61 ../ui/prefs_gtk3.ui.h:61
msgid "Signed"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:62 ../ui/prefs_gtk3.ui.h:62
msgid "Show separator"
msgstr "Erakutsi bereizlea"

#: ../ui/prefs_gtk2.ui.h:63 ../ui/prefs_gtk3.ui.h:63
msgid "Number of bits: "
msgstr "Bit kopurua:"

#: ../ui/prefs_gtk2.ui.h:64 ../ui/prefs_gtk3.ui.h:64
msgid "<b>Octal Settings</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:65 ../ui/prefs_gtk3.ui.h:65
msgid "<b>Binary Settings</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:66 ../ui/prefs_gtk3.ui.h:66
msgid "Fix length to a multiple of"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:67 ../ui/prefs_gtk3.ui.h:67 ../ui/prefs-ume.ui.h:21
msgid "Numbers"
msgstr "Zenbakiak"

#: ../ui/prefs_gtk2.ui.h:68 ../ui/prefs_gtk3.ui.h:68 ../ui/prefs-ume.ui.h:22
msgid "<b>RPN</b>"
msgstr "<b>RPN</b>"

#: ../ui/prefs_gtk2.ui.h:69 ../ui/prefs_gtk3.ui.h:69 ../ui/prefs-ume.ui.h:23
msgid "finite stack size (x, y, z, t)"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:70 ../ui/prefs_gtk3.ui.h:70 ../ui/prefs-ume.ui.h:24
msgid "infinite stack size"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:71 ../ui/prefs_gtk3.ui.h:71 ../ui/prefs-ume.ui.h:25
msgid "<b>Startup</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:72 ../ui/prefs_gtk3.ui.h:72 ../ui/prefs-ume.ui.h:26
msgid "Remember display value on exit"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:73 ../ui/prefs_gtk3.ui.h:73
msgid "<b>Visibility</b>"
msgstr ""

#: ../ui/prefs_gtk2.ui.h:74 ../ui/prefs_gtk3.ui.h:74
msgid "Show menu bar"
msgstr "Erakutsi menu-barra"

#: ../ui/prefs_gtk2.ui.h:75 ../ui/prefs_gtk3.ui.h:75 ../ui/prefs-ume.ui.h:27
msgid "General"
msgstr "Orokorra"

#: ../ui/scientific_buttons_gtk2.ui.h:1 ../ui/scientific_buttons_gtk3.ui.h:1
msgid "tan"
msgstr "tan"

#: ../ui/scientific_buttons_gtk2.ui.h:2 ../ui/scientific_buttons_gtk3.ui.h:2
msgid "Tangent"
msgstr "Tangentea"

#: ../ui/scientific_buttons_gtk2.ui.h:3 ../ui/scientific_buttons_gtk3.ui.h:3
msgid "ln"
msgstr "ln"

#: ../ui/scientific_buttons_gtk2.ui.h:4 ../ui/scientific_buttons_gtk3.ui.h:4
msgid "Natural logarithm (base: e)"
msgstr "Logaritmo naturala (oinarria: e)"

#: ../ui/scientific_buttons_gtk2.ui.h:7 ../ui/scientific_buttons_gtk3.ui.h:7
msgid "n!"
msgstr "n!"

#: ../ui/scientific_buttons_gtk2.ui.h:8 ../ui/scientific_buttons_gtk3.ui.h:8
msgid "Factorial"
msgstr "Faktoriala"

#: ../ui/scientific_buttons_gtk2.ui.h:9 ../ui/scientific_buttons_gtk3.ui.h:9
msgid "EE"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:10 ../ui/scientific_buttons_gtk3.ui.h:10
msgid "Enter exponent (base: 10)"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:11 ../ui/scientific_buttons_gtk3.ui.h:11
msgid "cos"
msgstr "cos"

#: ../ui/scientific_buttons_gtk2.ui.h:12 ../ui/scientific_buttons_gtk3.ui.h:12
msgid "Cosine"
msgstr "Kosinua"

#: ../ui/scientific_buttons_gtk2.ui.h:13 ../ui/scientific_buttons_gtk3.ui.h:13
msgid "log"
msgstr "log"

#: ../ui/scientific_buttons_gtk2.ui.h:14 ../ui/scientific_buttons_gtk3.ui.h:14
msgid "Logarithm (base: 10)"
msgstr "Logaritmoa (oinarria: 10)"

#: ../ui/scientific_buttons_gtk2.ui.h:15 ../ui/scientific_buttons_gtk3.ui.h:15
msgid "x^2"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:16 ../ui/scientific_buttons_gtk3.ui.h:16
msgid "Square"
msgstr "Karratua"

#: ../ui/scientific_buttons_gtk2.ui.h:19 ../ui/scientific_buttons_gtk3.ui.h:19
msgid "User defined functions"
msgstr "Erabiltzaileak definituriko funtzioak"

#: ../ui/scientific_buttons_gtk2.ui.h:20 ../ui/scientific_buttons_gtk3.ui.h:20
msgid "fun"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:22 ../ui/scientific_buttons_gtk3.ui.h:22
msgid "con"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:26 ../ui/scientific_buttons_gtk3.ui.h:26
msgid "inv"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:27 ../ui/scientific_buttons_gtk3.ui.h:27
msgid "Inverse mode"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:28 ../ui/scientific_buttons_gtk3.ui.h:28
msgid "hyp"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:29 ../ui/scientific_buttons_gtk3.ui.h:29
msgid "Hyperbolic mode"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:30 ../ui/scientific_buttons_gtk3.ui.h:30
msgid "sin"
msgstr "sin"

#: ../ui/scientific_buttons_gtk2.ui.h:31 ../ui/scientific_buttons_gtk3.ui.h:31
msgid "Sine"
msgstr "Sinua"

#: ../ui/scientific_buttons_gtk2.ui.h:32 ../ui/scientific_buttons_gtk3.ui.h:32
msgid "AND"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:33 ../ui/scientific_buttons_gtk3.ui.h:33
msgid "Logical AND"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:34 ../ui/scientific_buttons_gtk3.ui.h:34
msgid "OR"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:35 ../ui/scientific_buttons_gtk3.ui.h:35
msgid "Logical OR"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:36 ../ui/scientific_buttons_gtk3.ui.h:36
msgid "XOR"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:37 ../ui/scientific_buttons_gtk3.ui.h:37
msgid "Logical XOR"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:38 ../ui/scientific_buttons_gtk3.ui.h:38
msgid "CMP"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:39 ../ui/scientific_buttons_gtk3.ui.h:39
msgid "Bitwise complement"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:41 ../ui/scientific_buttons_gtk3.ui.h:41
msgid "Hexadecimal 0xC"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:42 ../ui/scientific_buttons_gtk3.ui.h:42
msgid "D"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:43 ../ui/scientific_buttons_gtk3.ui.h:43
msgid "Hexadecimal 0xD"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:44 ../ui/scientific_buttons_gtk3.ui.h:44
msgid "E"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:45 ../ui/scientific_buttons_gtk3.ui.h:45
msgid "Hexadecimal 0xE"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:46 ../ui/scientific_buttons_gtk3.ui.h:46
msgid "F"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:47 ../ui/scientific_buttons_gtk3.ui.h:47
msgid "Hexadecimal 0xF"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:48 ../ui/scientific_buttons_gtk3.ui.h:48
msgid "B"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:49 ../ui/scientific_buttons_gtk3.ui.h:49
msgid "Hexadecimal 0xB"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:50 ../ui/scientific_buttons_gtk3.ui.h:50
msgid "A"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:51 ../ui/scientific_buttons_gtk3.ui.h:51
msgid "Hexadecimal 0xA"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:60 ../ui/scientific_buttons_gtk3.ui.h:60
msgid "MOD"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:61 ../ui/scientific_buttons_gtk3.ui.h:61
msgid "Modulus"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:74 ../ui/scientific_buttons_gtk3.ui.h:74
msgid "LSH"
msgstr ""

#: ../ui/scientific_buttons_gtk2.ui.h:75 ../ui/scientific_buttons_gtk3.ui.h:75
msgid "Left shift"
msgstr ""
